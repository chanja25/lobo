//include
#include <windows.h>
#include <stdio.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <mmsystem.h>
//#include <Dxerr9.h>
#include "Font.h"

//マクロ定義
#define RELEASE(x)	{ if((x)){ (x)->Release(); (x) = NULL; } }

#define KEYCODE int


void KX_Init( HWND hWnd );
void KX_Release();

////////////////////////////////////////////////////////////////////////////////
//****************************************************************************//
//****************************DirectGraphics**********************************//
//****************************************************************************//
////////////////////////////////////////////////////////////////////////////////

//テクスチャー関連
typedef struct{
	//画像用
	LPDIRECT3DTEXTURE9 pTexture;
	//画像の座標用
	float posx,posy;
	float dx,dy;
	//画像のサイズ
	float width,height;

	//その他
	int flg;
	int step;
	int kind;
	int timer;

}KX2DOBJ,*LPKX2DOBJ;



enum{
	COLORKEY_NONE,
	COLORKEY_BLACK,
	COLORKEY_LEFTTOP,
};

//KX2DOBJ
//2DOBJのロード
LPKX2DOBJ KX_Load2DObject( char *filename, int fg = COLORKEY_NONE );
//画像描画
void KX_Render2DObject(
		float x,
		float y,
		int w,
		int h,
		LPKX2DOBJ pObj,
		long ScrX,
		long ScrY,
		long ScrW,
		long ScrH,
		D3DCOLOR argb = 0xffffffff
	);
void KX_Render2DObject(
		int x,
		int y,
		int w,
		int h,
		LPKX2DOBJ pObj,
		long ScrX,
		long ScrY,
		long ScrW,
		long ScrH,
		D3DCOLOR argb = 0xffffffff
	);
//テクスチャーの解放
void KX_Release2DObject( LPKX2DOBJ p2DOBJ );


//テクスチャー用

BOOL KX_Create( HWND hWnd );

void KX_Begin();
void KX_End();
void KX_BeginSprite();
LPDIRECT3DTEXTURE9 KX_LoadTexture( char *filename, int fg = COLORKEY_NONE );
void KX_ReleaseTexture( LPDIRECT3DTEXTURE9 &pTexture );
void KX_DrawTexture(
		int x,
		int y,
		int w,
		int h,
		LPDIRECT3DTEXTURE9 pTexture,
		long ScrX,
		long ScrY,
		long ScrW,
		long ScrH,
		D3DCOLOR argb = 0xffffffff
		);


//マップ
typedef struct{
	LPDIRECT3DTEXTURE9 pTexture;
	int MapData[32][32];
	int MapState[64];
}MAPDATA;

void KX_MapEdit( MAPDATA *Map, LPDIRECT3DTEXTURE9 pSrcTexture, int numX, int numY );

////////////////////////////////////////////////////////////////////////////////
//****************************************************************************//
//*****************************DirectSound************************************//
//****************************************************************************//
////////////////////////////////////////////////////////////////////////////////

BOOL InitAudio( HWND hWnd );
void ReleaseAudio();

BOOL LoadMusic( int no, CHAR filename[] );
BOOL PlayMusic( int no, int roop );
BOOL ChangeMusic( int no );
void StopMusic( int no );
void StopAllMusic();
BOOL SetVolumeMusic( long Volume );
BOOL SetMasterVolumeMusic( long Volume );

void LoadWave( int no, char *filename );
void PlayWave( int no, BOOL roop );
void SetStop( int no );
long GetVolume( int no );
void SetVolume( int no, long  pan );
long GetPan( int no );
void SetPan( int no, long  pan );


void LoadMidiFile( LPSTR filename );
void PlayMidi( bool loop );
void StopMidi();
void CloseMidi();


////////////////////////////////////////////////////////////////////////////////
//****************************************************************************//
//*****************************DirectShow*************************************//
//****************************************************************************//
////////////////////////////////////////////////////////////////////////////////

BOOL InitShow( HWND hWnd );
void SizeMedia( HWND hWnd );
void ReleaseShow();

void LoadMedia( LPSTR filename );
void PlayMedia();
void StopMedia();
void PauseMedia();
void Volume();

////////////////////////////////////////////////////////////////////////////////
//****************************************************************************//
//*****************************DirectInput************************************//
//****************************************************************************//
////////////////////////////////////////////////////////////////////////////////

BOOL InitDinput(HWND hWnd);
void InputRelease();

//キー情報処理
void KeySetInfo();
KEYCODE KEY(KEYCODE KeyMsg);
KEYCODE MOUSE(KEYCODE KeyMsg);
KEYCODE JOY(KEYCODE KeyMsg);

typedef struct{
	int key;
	int code;
}KEY_ASSIGN;

typedef struct{
	BOOL (* GetKeyFunk)(int,int);
	KEY_ASSIGN *pAssign;
}DEVICE_LIST;

#define PAD_MAX 4

enum{
	KEY_UP,	KEY_DOWN, KEY_LEFT,	KEY_RIGHT, KEY_ENTER, KEY_SPACE, KEY_LCONTROL, KEY_RCONTROL, KEY_LSHIFT, KEY_RSHIFT,
	KEY_Q, KEY_W, KEY_E, KEY_R, KEY_T, KEY_Y, KEY_U, KEY_I, KEY_O, KEY_P,
	KEY_A, KEY_S, KEY_D, KEY_F, KEY_G, KEY_H, KEY_J, KEY_K, KEY_L,
	KEY_Z, KEY_X, KEY_C, KEY_V, KEY_B, KEY_N, KEY_M,
	KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9, KEY_0,
	KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10, KEY_F11, KEY_F12,
	KEY_MAX,
};

enum{
	//マウス用
	MOUSE_LEFT,
	MOUSE_RIGHT,
	MOUSE_CENTER,
	MOUSE_MAX,
	//ジョイスティック用
	JOY_01 = 0, JOY_02, JOY_03, JOY_04,
	JOY_05, JOY_06, JOY_07, JOY_08,
	JOY_09, JOY_10, JOY_11, JOY_12,
	JOY_13, JOY_14, JOY_15, JOY_16,
	JOY_17, JOY_18, JOY_19, JOY_20,
	JOY_21, JOY_22, JOY_23, JOY_24,
	JOY_25, JOY_26, JOY_27, JOY_28,
	JOY_29, JOY_30, JOY_31, JOY_32,
	JOY_UP, JOY_DOWN, JOY_LEFT, JOY_RIGHT,
	JOY_MAX,
};