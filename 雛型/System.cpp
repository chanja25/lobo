//include
#include "KX.h"
#include "system.h"

int dwMainMode;
UINT GameCnt;

void SYS_Initialize()
{
	GameCnt = 0;
	TITLE_Initialize();
}

BOOL SYS_Proc()
{
	GameCnt++;

	KeySetInfo();
	switch(dwMainMode){
		case MAIN_MODE:	MAIN_Proc();	break;
		case TITLE_MODE:TITLE_Proc();	break;
		case CLEAR_MODE:CLEAR_Proc();	break;
		case END_MODE:	END_Proc();		break;
	}
	return TRUE;
}

void SYS_Draw()
{
	KX_Begin();
	switch(dwMainMode){
		case MAIN_MODE:	MAIN_Draw();	break;
		case TITLE_MODE:TITLE_Draw();	break;
		case CLEAR_MODE:CLEAR_Draw();	break;
		case END_MODE:	END_Draw();	break;
	}
	KX_End();
}