#include "Game.h"

#define	HP_MAX		1500
#define	ENERGY_MAX	1000

extern int dwMainMode;
extern UINT GameCnt;

//モード選択用
#define TITLE_MODE 0
#define MAIN_MODE 1
#define CLEAR_MODE 2
#define END_MODE 255

void SYS_Initialize();
BOOL SYS_Proc();
void SYS_Draw();

void MAIN_Initialize();
void MAIN_Release();
void MAIN_Proc();
void MAIN_Draw();

void TITLE_Initialize();
void TITLE_Proc();
void TITLE_Draw();

void END_Initialize();
void END_Proc();
void END_Draw();

void CLEAR_Initialize();
void CLEAR_Proc();
void CLEAR_Draw();