
#define ENEMY_MAX 50

//ステージ用
void STAGE_Initialize();
void STAGE_Release();
void STAGE_Proc();
void STAGE_Draw();

void CheckWallUp( int *x, int *y, int *MoveY );
void CheckWallDown( int *x, int *y, int *MoveY );
void CheckWallLeft( int *x, int *y, int *MoveX );
void CheckWallRight( int *x, int *y, int *MoveX );

//プレイヤー用
void PLAYER_Initialize();
void PLAYER_Release();
void PLAYER_Proc();
void PLAYER_Draw();

void DISP_Initialize();
void DISP_Release();
void DISP_Proc();
void DISP_Draw();

void SHOT_Create( int x, int y, int Angle, int State, int type );
void SHOT_Initialize();
void SHOT_Release();
void SHOT_Proc();
void SHOT_Draw();

void ENEMY_Initialize();
void ENEMY_Release();
void ENEMY_Create( int x, int y, int type );
void ENEMY_Proc();
void ENEMY_Draw();