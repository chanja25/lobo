#include "KX.h"
#include "system.h"

LPKX2DOBJ lpTitle = NULL;
LPKX2DOBJ lpMoji = NULL;

int tcnt;

//初期化
void TITLE_Initialize()
{
	lpTitle = KX_Load2DObject( "DATA\\TITLE.png" );
	lpMoji = KX_Load2DObject( "DATA\\ENTER.png" );

	tcnt = 0;

	dwMainMode = TITLE_MODE;
}

void TITLE_Release()
{
	KX_Release2DObject( lpTitle );
	KX_Release2DObject( lpMoji );

	MAIN_Initialize();
}

//処理
void TITLE_Proc()
{
	tcnt++;
	if( KEY(KEY_ENTER)==3 ){
		TITLE_Release();
	}
}

//描画
void TITLE_Draw()
{
	KX_Render2DObject( 0, 0, 640, 480, lpTitle, 0, 0, 640, 480 );
	if( tcnt%30<15 )KX_Render2DObject( 200, 320, 240, 64, lpMoji, 0, 0, 240, 64 );
}