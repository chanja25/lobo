//lib
#pragma comment( lib, "dinput8.lib" )
#pragma comment( lib, "dxguid.lib" )

//include
#include <dinput.h>

#include "KX.h"
#include "system.h"

KEYCODE MouseInfo[MOUSE_MAX];
KEYCODE KeyInfo[KEY_MAX];
KEYCODE JoyInfo[JOY_MAX];

//DirectInput
LPDIRECTINPUT8			pDinput = NULL;
//KeyBoard用デバイス
LPDIRECTINPUTDEVICE8	pKeyDevice = NULL;
//Mouse用デバイス
LPDIRECTINPUTDEVICE8	pMouseDevice = NULL;
//JoyStick用デバイス
LPDIRECTINPUTDEVICE8	pJoyDevice[4];
int JoyNum;

LONG mouseX,mouseY;

int KeyMap[KEY_MAX] = {
	DIK_UP, DIK_DOWN, DIK_LEFT, DIK_RIGHT, DIK_RETURN, DIK_SPACE, DIK_LCONTROL, DIK_RCONTROL, DIK_LSHIFT, DIK_RSHIFT,
	DIK_Q, DIK_W, DIK_E, DIK_R, DIK_T, DIK_Y, DIK_U, DIK_I, DIK_O, DIK_P,
	DIK_A, DIK_S, DIK_D, DIK_F, DIK_G, DIK_H, DIK_J, DIK_K, DIK_L,
	DIK_Z, DIK_X, DIK_C, DIK_V, DIK_B, DIK_N, DIK_M,
	DIK_1, DIK_2, DIK_3, DIK_4, DIK_5, DIK_6, DIK_7, DIK_8, DIK_9, DIK_0,
	DIK_F1, DIK_F2, DIK_F3, DIK_F4, DIK_F5, DIK_F6, DIK_F7, DIK_F8, DIK_F9, DIK_F10, DIK_F11, DIK_F12
};

BOOL CALLBACK EnumJoyCallBack( const DIDEVICEINSTANCE *pdidInstance, VOID *pContext );
BOOL CALLBACK EnumAxesCallBack( LPCDIDEVICEOBJECTINSTANCE lpddoi, LPVOID pvRef );

//DirectInputの初期化
BOOL InitDinput(HWND hWnd)
{
	HRESULT hr;
	//キーボード用
	//「DirectInput」オブジェクトの作成
	hr = DirectInput8Create( GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDinput, NULL );
	if( FAILED( hr ) )return false;
	//「DurectInputデバイス」オブジェクトの作成
	hr = pDinput->CreateDevice( GUID_SysKeyboard, &pKeyDevice, NULL );
	if( FAILED( hr ) )return false;
	//デバイスをキーボードに設定
	hr = pKeyDevice->SetDataFormat( &c_dfDIKeyboard );
	if( FAILED( hr ) )return false;
	//協調レベルの設定
	hr = pKeyDevice->SetCooperativeLevel( hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND );
	if( FAILED( hr ) )return false;
	//デバイスを取得する
	pKeyDevice->Acquire();

	//マウス用
	//「DurectInputデバイス」オブジェクトの作成
	hr = pDinput->CreateDevice( GUID_SysMouse, &pMouseDevice, NULL );
	if( FAILED( hr ) )return false;
	//デバイスをキーボードに設定
	hr = pMouseDevice->SetDataFormat( &c_dfDIMouse2 );
	if( FAILED( hr ) )return false;
	//協調レベルの設定
	hr = pMouseDevice->SetCooperativeLevel( hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND );
	if( FAILED( hr ) )return false;
	/*
	//デバイス入力データの設定
	DIPROPDWORD diprop;

	diprop.diph.dwSize = sizeof( diprop );
	diprop.diph.dwHeaderSize = sizeof( diprop.diph );
	diprop.diph.dwObj = 0;
	diprop.diph.dwHow = DIPH_DEVICE;
	diprop.dwData = DIPROPAXISMODE_REL;
	hr = pMouseDevice->SetProperty( DIPROP_AXISMODE, &diprop.diph );
	if( FAILED( hr ) )return false;
	//デバイスを取得する
	pMouseDevice->Acquire();
	*/
	//マウスの初期座標
	mouseX = 320;
	mouseY = 240;

	//ジョイスティック用
	JoyNum = 0;
	pDinput->EnumDevices( DI8DEVCLASS_GAMECTRL, EnumJoyCallBack, &hWnd, DIEDFL_ATTACHEDONLY );

	for(int i=0;i<KEY_MAX;i++)KeyInfo[i] = 0;

	return true;
}

BOOL CALLBACK EnumJoyCallBack( const DIDEVICEINSTANCE *pdidInstance, VOID *pContext )
{
	HRESULT hr;

	if(JoyNum >= 4 )return DIENUM_STOP;

	//「DurectInputデバイス」オブジェクトの作成
	hr = pDinput->CreateDevice( pdidInstance->guidInstance, &pJoyDevice[JoyNum], NULL );
	if( FAILED( hr ) )return false;
	//デバイスをキーボードに設定
	hr = pJoyDevice[JoyNum]->SetDataFormat( &c_dfDIJoystick2 );
	if( FAILED( hr ) )return false;
	//協調レベルの設定
	hr = pJoyDevice[JoyNum]->SetCooperativeLevel( *((HWND *)pContext), DISCL_NONEXCLUSIVE | DISCL_BACKGROUND );
	if( FAILED( hr ) ){
		RELEASE( pJoyDevice[JoyNum] );
		return false;
	}

	hr = pJoyDevice[JoyNum]->EnumObjects( EnumAxesCallBack, NULL, DIDFT_AXIS );
	if( FAILED( hr ) ){
		RELEASE( pJoyDevice[JoyNum] );
		return false;
	}
	//デバイスを取得する
	pJoyDevice[JoyNum]->Acquire();

	JoyNum++;
	return DIENUM_CONTINUE;
}

BOOL CALLBACK EnumAxesCallBack( LPCDIDEVICEOBJECTINSTANCE lpddoi, LPVOID pvRef )
{
	HRESULT hr;
	//デバイス入力データの設定
	DIPROPRANGE diprg;

	ZeroMemory( &diprg , sizeof( diprg ) );
	diprg.diph.dwSize = sizeof( diprg );
	diprg.diph.dwHeaderSize = sizeof( diprg.diph );
	diprg.diph.dwObj = lpddoi->dwType;
	diprg.diph.dwHow = DIPH_BYID;
	diprg.lMin = -1000;
	diprg.lMax = 1000;
	hr = pJoyDevice[JoyNum]->SetProperty( DIPROP_RANGE, &diprg.diph );

	if( FAILED( hr ) )return DIENUM_STOP;
	return DIENUM_CONTINUE;
}

//キーデバイスの解放
void InputRelease()
{

	//キーボードデバイスの解放
	if( pKeyDevice ){
		pKeyDevice->Unacquire();
		RELEASE( pKeyDevice );
	}
	//ジョイスティックデバイスの解放
	if( pMouseDevice ){
		pMouseDevice->Unacquire();
		RELEASE( pMouseDevice );
	}
	//ジョイスティックデバイスの解放
	for(int i=0;i<JoyNum;i++ ){
		pJoyDevice[i]->Unacquire();
		RELEASE( pJoyDevice[i] );
	}

	//DirectInputインターフェースの解放
	RELEASE( pDinput );
}

BOOL GetKeyState( int no, int code )
{
	HRESULT hr;
	//キーボードで押されているキーを調べる
	BYTE disk[256];
	hr = pKeyDevice->GetDeviceState(sizeof(disk),&disk);
	if( hr==DIERR_INPUTLOST )pKeyDevice->Acquire();
	if( disk[code] & 0x80 )return true;
	return false;
}

BOOL GetMouseState( int no, int code )
{
	HRESULT hr;
	DIMOUSESTATE2 dims;
	hr = pMouseDevice->GetDeviceState(sizeof(dims),&dims);
	if( hr==DIERR_INPUTLOST )pMouseDevice->Acquire();
	if( dims.rgbButtons[code] & 0x80 )return true;
	return false;
}

BOOL GetJoyState( int no, int code )
{
	HRESULT hr;
	if( no<JoyNum ){
		DIJOYSTATE2 dijs;
		pJoyDevice[no]->Poll();
		hr = pJoyDevice[no]->GetDeviceState(sizeof(dijs),&dijs);
		if( hr==DIERR_INPUTLOST )pJoyDevice[no]->Acquire();

		if( code==32 )return dijs.lY <= -1000;
		if( code==33 )return dijs.lY >= 1000;
		if( code==34 )return dijs.lX <= -1000;
		if( code==35 )return dijs.lX >= 1000;
		if( dijs.rgbButtons[code] & 0x80 )return true;
	}
	return false;
}


//DirectInput用
void DirectInput()
{
	KEYCODE Key;

	for(int i=0;i<KEY_MAX;i++){
		if( GetKeyState( 0, KeyMap[i] ) )Key = 1;else Key = 0;

		if( KeyInfo[i] & 0x01){if(Key)KeyInfo[i] = 1; else KeyInfo[i] = 2;}
		else							 {if(Key)KeyInfo[i] = 3; else KeyInfo[i] = 0;}
	}

	for(int i=0;i<MOUSE_MAX;i++){
		if( GetMouseState( 0, i ) )Key = 1;else Key = 0;

		if( MouseInfo[i] & 0x01){if(Key)MouseInfo[i] = 1; else MouseInfo[i] = 2;}
		else							 {if(Key)MouseInfo[i] = 3; else MouseInfo[i] = 0;}
	}

	for(int i=0;i<JOY_MAX;i++){
		if( GetJoyState( 0, i ) )Key = 1;else Key = 0;

		if( JoyInfo[i] & 0x01){if(Key)JoyInfo[i] = 1; else JoyInfo[i] = 2;}
		else							 {if(Key)JoyInfo[i] = 3; else JoyInfo[i] = 0;}
	}
}

//キーの情報設定
void KeySetInfo()
{
	DirectInput();
}

//キー入力
KEYCODE KEY(KEYCODE KeyMsg)
{
	return KeyInfo[KeyMsg];
}

KEYCODE MOUSE(KEYCODE KeyMsg)
{
	return MouseInfo[KeyMsg];
}

KEYCODE JOY(KEYCODE KeyMsg)
{
	return JoyInfo[KeyMsg];
}

LONG MousePosX()
{
	return mouseX;
}

LONG MousePosY()
{
	return mouseY;
}