//include
#include "KX.h"
#include "system.h"

float Length2D( float x1, float y1, float x2, float y2 )
{
	float x,y;
	x = x1 - x2;
	y = y1 - y2;
	return sqrtf( x*x + y*y );
}

float Length3D( float x1, float y1, float z1, float x2, float y2, float z2 )
{
	float x,y,z;
	x = x1 - x2;
	y = y1 - y2;
	z = z1 - z2;
	return sqrtf( x*x + y*y + z*z );
}
