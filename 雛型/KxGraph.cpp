//lib
#pragma comment( lib, "d3d9.lib" )
#pragma comment( lib, "d3dx9.lib" )

//include
#include "KX.h"
#include "system.h"


//変数定義
LPDIRECT3D9 pD3D;
LPDIRECT3DDEVICE9 pD3DDevice;

LPD3DXSPRITE pSprite;

//DirectGraphicsの初期化処理//
BOOL KX_Create( HWND hWnd )
{
	HRESULT hr;

	pD3D = Direct3DCreate9( D3D_SDK_VERSION );
	if( pD3D == NULL )return FALSE;

	D3DPRESENT_PARAMETERS pD3DPP;
	ZeroMemory( &pD3DPP, sizeof( pD3DPP ) );
	pD3DPP.Windowed						= TRUE;
	pD3DPP.SwapEffect					= D3DSWAPEFFECT_DISCARD;
	pD3DPP.BackBufferFormat				= D3DFMT_UNKNOWN;
	pD3DPP.BackBufferCount				= 1;
	pD3DPP.BackBufferWidth				= 640;
	pD3DPP.BackBufferHeight				= 480;
	pD3DPP.EnableAutoDepthStencil		= TRUE;
	pD3DPP.AutoDepthStencilFormat		= D3DFMT_D16;

	hr = pD3D->CreateDevice(
			D3DADAPTER_DEFAULT,
			D3DDEVTYPE_HAL,
			hWnd,
			D3DCREATE_MIXED_VERTEXPROCESSING,
			&pD3DPP,
			&pD3DDevice
		);
	if( FAILED( hr ) ){
		if( FAILED( pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, hWnd,
										D3DCREATE_MIXED_VERTEXPROCESSING, &pD3DPP, &pD3DDevice ) ) ){
			return FALSE;
		}
	}
	hr = D3DXCreateSprite( pD3DDevice, &pSprite );
	if( FAILED( hr ) )return FALSE;

	return TRUE;
}

void KX_Init( HWND hWnd )
{
	KX_Create( hWnd );
	InitDinput( hWnd );
	InitShow( hWnd );
	InitAudio( hWnd );
	InitFont();
}
//DirectXの解放処理//
void KX_Release()
{
	ReleaseFont();
	ReleaseAudio();
	ReleaseShow();
	InputRelease();

	RELEASE( pSprite );

	RELEASE( pD3DDevice );
	RELEASE( pD3D );
}

//DirectGraphicsの描画関連//

//シーンのクリア
void KX_ClearScene( int r, int g, int b )
{
	pD3DDevice->Clear( 0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(r,g,b), 1.0f, 0 );
}

//描画開始
void KX_BeginScene()
{
	pD3DDevice->BeginScene();
}

//描画終了
void KX_EndScene()
{
	pD3DDevice->EndScene();
}

//シーンの表示
void KX_DispScene()
{
	pD3DDevice->Present( NULL, NULL, NULL, NULL );
}


//スプライト関連//

//スプライトの描画開始
void KX_BeginSprite()
{
	pSprite->Begin( D3DXSPRITE_ALPHABLEND );
}

//スプライトの描画
void KX_DrawSprite( LPDIRECT3DTEXTURE9 pTexture, long left, long top, long weight, long height, float OfsX, float OfsY, D3DCOLOR argb )
{
	RECT rect;
	D3DXVECTOR3 pos;

	rect.left = left;
	rect.top = top;
	rect.right = left + weight;
	rect.bottom = top + height;

	pos.x = OfsX;
	pos.y = OfsY;
	pos.z = .0f;

	pSprite->Draw( pTexture, &rect, NULL, &pos, argb);
}

//スプライトの描画終了
void KX_EndSprite()
{
	pSprite->End();
}

//描画前
void KX_Begin()
{
	KX_ClearScene(0,0,0);
	KX_BeginScene();
	KX_BeginSprite();
}

//描画後
void KX_End()
{
	KX_EndSprite();
	KX_EndScene();
	KX_DispScene();
}

//画像描画
void KX_DrawTexture(
		int x,
		int y,
		int w,
		int h,
		LPDIRECT3DTEXTURE9 pTexture,
		long ScrX,
		long ScrY,
		long ScrW,
		long ScrH,
		D3DCOLOR argb
	)
{

	float ScaleX,ScaleY;
	ScaleX = (float)w/ScrW;
	ScaleY = (float)h/ScrH;

	D3DXMATRIX matTrans;
	D3DXMATRIX matDummy;

	D3DXMatrixIdentity( &matTrans );

	D3DXMatrixScaling( &matDummy, ScaleX, ScaleY, 1.0f );
	matTrans*=matDummy;

	D3DXMatrixTranslation( &matDummy, (float)x, (float)y, 0 );
	matTrans*=matDummy;

	pSprite->SetTransform( &matTrans );

	KX_DrawSprite( pTexture, ScrX, ScrY, ScrW, ScrH, .0f, .0f, argb );
}

//テクスチャー関連//

//テクスチャーの作成
LPDIRECT3DTEXTURE9 KX_CreateTexture( UINT Width, UINT Height )
{
	LPDIRECT3DTEXTURE9 pTexture;
	HRESULT hr;
	hr = D3DXCreateTexture( pD3DDevice, Width, Height, 1, 0, D3DFMT_A1R5G5B5, D3DPOOL_MANAGED, &pTexture );
	if( FAILED( hr ) )return NULL;
	return pTexture;
}

//テクスチャーのロード
LPDIRECT3DTEXTURE9 KX_LoadTexture( LPSTR filename, int fg )
{
	LPDIRECT3DTEXTURE9	pTmpTexture;
	IDirect3DSurface9	*pTmpSurface;
	D3DSURFACE_DESC		desc;

	LPDIRECT3DTEXTURE9	pOrgTexture;
	IDirect3DSurface9	*pOrgSurface;

	D3DCOLOR			ColorKey;
	D3DLOCKED_RECT		LockedRect;

	HRESULT hr;

	hr = D3DXCreateTextureFromFileEx(
			pD3DDevice,
			filename,
			D3DX_DEFAULT,
			D3DX_DEFAULT,
			1,
			0,
			D3DFMT_A8R8G8B8,
			D3DPOOL_SYSTEMMEM,
			D3DX_FILTER_NONE,
			D3DX_FILTER_NONE,
			0,
			NULL,
			NULL,
			&pTmpTexture
		);
	if( FAILED( hr ) )return NULL;

	pTmpTexture->GetLevelDesc( 0, &desc );
	pTmpTexture->GetSurfaceLevel( 0, &pTmpSurface );

	switch( fg ){
		case COLORKEY_BLACK:
			ColorKey = D3DCOLOR_ARGB( 255, 0, 0, 0 );
			break;
		case COLORKEY_LEFTTOP:
			pTmpSurface->LockRect( &LockedRect, NULL, D3DLOCK_READONLY );
			ColorKey = *((D3DCOLOR *)LockedRect.pBits);
			pTmpSurface->UnlockRect();
			break;
		default:
			ColorKey = 0;
			break;
	}
	pOrgTexture = KX_CreateTexture( desc.Width, desc.Height );
	pOrgTexture->GetSurfaceLevel( 0, &pOrgSurface );

	D3DXLoadSurfaceFromSurface( pOrgSurface, NULL, NULL, pTmpSurface, NULL, NULL, D3DX_FILTER_NONE, ColorKey );

	RELEASE( pOrgSurface );
	RELEASE( pTmpSurface );
	RELEASE( pTmpTexture );

	return pOrgTexture;
}

//テクスチャー間転送
void KX_TextureBlt( LPDIRECT3DTEXTURE9 pDstTexture, LONG DstX, LONG DstY, LONG Width, LONG Height,
												LPDIRECT3DTEXTURE9 pSrcTexture, LONG SrcX, LONG SrcY )
{
	IDirect3DSurface9 *pSrcSurface;
	IDirect3DSurface9 *pDstSurface;
	RECT	SrcRect;
	RECT	DstRect;

	SetRect( &SrcRect, SrcX, SrcY, SrcX+Width, SrcY+Height );
	SetRect( &DstRect, DstX, DstY, DstX+Width, DstY+Height );

	pSrcTexture->GetSurfaceLevel( 0, &pSrcSurface );
	pDstTexture->GetSurfaceLevel( 0, &pDstSurface );

	D3DXLoadSurfaceFromSurface( pDstSurface, NULL, &DstRect, pSrcSurface, NULL, &SrcRect, D3DX_FILTER_NONE, 0 );

	RELEASE( pSrcSurface );
	RELEASE( pDstSurface );
}

//テクスチャーの解放
void KX_ReleaseTexture( LPDIRECT3DTEXTURE9 &pTexture )
{
	RELEASE( pTexture );
}

void KX_MapEdit( MAPDATA *Map, LPDIRECT3DTEXTURE9 pSrcTexture, int numX, int numY )
{
	Map->pTexture = KX_CreateTexture( numX*32, numY*32 );

	for(int i=0;i<numX;i++){
		for(int j=0;j<numY;j++){
			KX_TextureBlt( Map->pTexture, i*32, j*32, 32, 32,
				pSrcTexture, Map->MapData[j][i]%8*32, Map->MapData[j][i]/8*32 ); 
		}
	}
}

//KX2DOBJ
//2DOBJのロード
LPKX2DOBJ KX_Load2DObject( LPSTR filename, int fg )
{
	LPKX2DOBJ			lpObj;
	lpObj = new KX2DOBJ;
	ZeroMemory( lpObj, sizeof(KX2DOBJ) );

	LPDIRECT3DTEXTURE9	pTmpTexture;
	IDirect3DSurface9	*pTmpSurface;
	D3DSURFACE_DESC		desc;

	IDirect3DSurface9	*pOrgSurface;

	D3DCOLOR			ColorKey;
	D3DLOCKED_RECT		LockedRect;

	HRESULT hr;

	hr = D3DXCreateTextureFromFileEx(
			pD3DDevice,
			filename,
			D3DX_DEFAULT,
			D3DX_DEFAULT,
			1,
			0,
			D3DFMT_A8R8G8B8,
			D3DPOOL_SYSTEMMEM,
			D3DX_FILTER_NONE,
			D3DX_FILTER_NONE,
			0,
			NULL,
			NULL,
			&pTmpTexture
		);
	if( FAILED( hr ) )return NULL;

	pTmpTexture->GetLevelDesc( 0, &desc );
	pTmpTexture->GetSurfaceLevel( 0, &pTmpSurface );

	switch( fg ){
		case COLORKEY_BLACK:
			ColorKey = D3DCOLOR_ARGB( 255, 0, 0, 0 );
			break;
		case COLORKEY_LEFTTOP:
			pTmpSurface->LockRect( &LockedRect, NULL, D3DLOCK_READONLY );
			ColorKey = *((D3DCOLOR *)LockedRect.pBits);
			pTmpSurface->UnlockRect();
			break;
		default:
			ColorKey = 0;
			break;
	}
	lpObj->pTexture = KX_CreateTexture( desc.Width, desc.Height );
	lpObj->pTexture->GetSurfaceLevel( 0, &pOrgSurface );

	D3DXLoadSurfaceFromSurface( pOrgSurface, NULL, NULL, pTmpSurface, NULL, NULL, D3DX_FILTER_NONE, ColorKey );


	RELEASE( pOrgSurface );
	RELEASE( pTmpSurface );
	RELEASE( pTmpTexture );

	return lpObj;
}

//画像描画
void KX_Render2DObject(
		int x,
		int y,
		int w,
		int h,
		LPKX2DOBJ lpObj,
		long ScrX,
		long ScrY,
		long ScrW,
		long ScrH,
		D3DCOLOR argb
	)
{

	float ScaleX,ScaleY;
	ScaleX = (float)w/ScrW;
	ScaleY = (float)h/ScrH;

	D3DXMATRIX matTrans;
	D3DXMATRIX matDummy;

	D3DXMatrixIdentity( &matTrans );

	D3DXMatrixScaling( &matDummy, ScaleX, ScaleY, 1.0f );
	matTrans*=matDummy;

	D3DXMatrixTranslation( &matDummy, (float)x, (float)y, 0 );
	matTrans*=matDummy;

	pSprite->SetTransform( &matTrans );

	KX_DrawSprite( lpObj->pTexture, ScrX, ScrY, ScrW, ScrH, .0f, .0f, argb );
}


//画像描画
void KX_Render2DObject(
		float x,
		float y,
		int w,
		int h,
		LPKX2DOBJ lpObj,
		long ScrX,
		long ScrY,
		long ScrW,
		long ScrH,
		D3DCOLOR argb
	)
{

	float ScaleX,ScaleY;
	ScaleX = (float)w/ScrW;
	ScaleY = (float)h/ScrH;

	D3DXMATRIX matTrans;
	D3DXMATRIX matDummy;

	D3DXMatrixIdentity( &matTrans );

	D3DXMatrixScaling( &matDummy, ScaleX, ScaleY, 1.0f );
	matTrans*=matDummy;

	D3DXMatrixTranslation( &matDummy, x, y, 0 );
	matTrans*=matDummy;

	pSprite->SetTransform( &matTrans );

	KX_DrawSprite( lpObj->pTexture, ScrX, ScrY, ScrW, ScrH, .0f, .0f, argb );
}

//KX2DOBJの解放
void KX_Release2DObject( LPKX2DOBJ p2DOBJ )
{
	if( !p2DOBJ )return;
	if( p2DOBJ->pTexture )RELEASE( p2DOBJ->pTexture );
	delete p2DOBJ;
}