#include "KX.h"
#include "system.h"


LPKX2DOBJ lpEnemy;
int ex[ENEMY_MAX],ey[ENEMY_MAX];
int emx[ENEMY_MAX], emy[ENEMY_MAX];
int eangle[ENEMY_MAX];
int estate[ENEMY_MAX];
int ehp[ENEMY_MAX];
int emotion[ENEMY_MAX];
int etype[ENEMY_MAX];

//初期化
void ENEMY_Initialize()
{
	lpEnemy = KX_Load2DObject( "DATA\\Robo.png", COLORKEY_LEFTTOP );
	for( int i=0;i<ENEMY_MAX;i++ ){
		ex[i] = ey[i] = 0;
		emx[i] = emy[i] = 0;
		eangle[i] = 0;
		estate[i] = 0;
		emotion[i] = 0;
	}
}

void ENEMY_Release()
{
	KX_Release2DObject( lpEnemy );
}

void ENEMY_Create( int x, int y,int type )
{
	for( int i=0;i<ENEMY_MAX;i++ ){
		if( estate[i]==0 ){
			ex[i] = x;
			ey[i] = y;
			estate[i] = 1;
			ehp[i] = 100;
			etype[i] = type;
			break;
		}
	}
}

void ENEMY_Move( int n )
{
	if( ehp[n]<=0 )estate[n] = 0;

	ex[n] += emx[n];
	ey[n] += emy[n];

	extern int x;
	if( abs(x-ex[n])<600 && abs(x-ex[n])>200 && etype[n]==0  )emx[n]--;
	else if( abs(x-ex[n])<200 )emx[n]++;

	if( emx[n]<-8)emx[n] = -8;
	if( emx[n]>0 )emx[n] = 0;

	if( emx[n]!=0 )emotion[n] = 0;
	else if( GameCnt%5==0 && rand()%5==0 ){
		emotion[n] = 2;
		SHOT_Create( ex[n], ey[n], eangle[n], 1, 1 );
	}else if( GameCnt%10==0 && rand()%5==0 ){
		emotion[n] = 7;
		SHOT_Create( ex[n], ey[n], eangle[n], 2, 1 );
	}


	if( ex[n]-x>0 )eangle[n] = 0;
	else eangle[n] = 1;

	if( ey[n]>540 )estate[n] = 0;
	emy[n]++;

	if( emy[n]>0 )CheckWallDown( &ex[n], &ey[n], &emy[n] );
	if( emy[n]<0 )CheckWallUp( &ex[n], &ey[n], &emy[n] );
	if( emx[n]<0 )CheckWallLeft( &ex[n], &ey[n], &emx[n] );
	if( emx[n]>0 )CheckWallRight( &ex[n], &ey[n], &emx[n] );
}

//処理
void ENEMY_Proc()
{
	for( int i=0;i<ENEMY_MAX;i++ ){
		if( estate[i] ){
			ENEMY_Move( i );
		}
	}
}

//描画
void ENEMY_Draw()
{
	for( int i=0;i<ENEMY_MAX;i++ ){
		if( estate[i] ){
			KX_Render2DObject( ex[i]-32, ey[i]-64, 64, 64, lpEnemy, emotion[i]*64, eangle[i]*64, 64, 64 );
		}
	}
}