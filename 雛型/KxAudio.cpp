#define DIRECTINPUT_VERSION 0x0800

//lib
#pragma comment( lib, "dsound.lib" )
#pragma comment( lib, "winmm.lib" )

//include
#include "KX.h"
#include "system.h"

#include <dsound.h>

//最大再生数
#define NUM 32


LPDIRECTSOUND lpDS;
LPDIRECTSOUNDBUFFER lpDSBuffer;
LPDIRECTSOUNDBUFFER lpDSSecond[NUM];

BOOL InitDirectSound( HWND hWnd );

BOOL InitAudio( HWND hWnd )
{
	if( ( InitDirectSound( hWnd ) )==FALSE ){
		return FALSE;
	}

	return TRUE;
}

//DirectSoundの初期化
BOOL InitDirectSound( HWND hWnd )
{
	//DirectSoundの初期化
	HRESULT hr;
	hr = DirectSoundCreate( NULL, &lpDS, NULL );
	if( FAILED( hr ) )return FALSE;

	//DirectSoundの協調レベルをセットする
	hr = lpDS->SetCooperativeLevel( hWnd, DSSCL_PRIORITY );
	if( FAILED( hr ) )return FALSE;


	//プライマリバッファの作成
	DSBUFFERDESC dsbdesc;

	//DSBUFFERDESC構造体の設定
	ZeroMemory( &dsbdesc, sizeof(DSBUFFERDESC) );
	dsbdesc.dwSize = sizeof(DSBUFFERDESC);
	dsbdesc.dwFlags = DSBCAPS_CTRLVOLUME | DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRLPAN;
	dsbdesc.dwBufferBytes = 0;
	dsbdesc.lpwfxFormat = NULL;

	//バッファの作成
	hr = lpDS->CreateSoundBuffer( &dsbdesc, &lpDSBuffer, NULL );
	if( FAILED( hr ) )return FALSE;

	//プライマリバッファのWaveフォーマットを設定
	WAVEFORMATEX pcmwf;
	ZeroMemory( &pcmwf, sizeof(WAVEFORMATEX) );
	pcmwf.wFormatTag = WAVE_FORMAT_PCM;
	pcmwf.nChannels = 2;
	pcmwf.nSamplesPerSec = 22050;
	pcmwf.nBlockAlign = 4;
	pcmwf.nAvgBytesPerSec = pcmwf.nSamplesPerSec * pcmwf.nBlockAlign;
	pcmwf.wBitsPerSample = 16;

	hr = lpDSBuffer->SetFormat( &pcmwf );
	if( FAILED( hr ) )return FALSE;
	return TRUE;
}

void ReleaseAudio()
{
	for(int i=0;i<NUM;i++){
		if( lpDSSecond[i]!=NULL )RELEASE( lpDSSecond[i] );
	}
	RELEASE( lpDS );
}

//WaveDataのセット
BOOL SetWave( HMMIO hmmio, int no )
{
	MMCKINFO mainChunk;
	MMCKINFO subChunk;
	WAVEFORMATEX wfx;
	char *wavedata;

	HRESULT hr;

	mainChunk.fccType = mmioFOURCC( 'W', 'A', 'V', 'E' );
	if( mmioDescend( hmmio, &mainChunk, NULL, MMIO_FINDRIFF ) ) return false;

	subChunk.ckid = mmioFOURCC( 'f', 'm', 't', ' ' );
	if( mmioDescend( hmmio, &subChunk, &mainChunk, MMIO_FINDCHUNK ) )return false;

	if( mmioRead( hmmio, (HPSTR)&wfx, sizeof(wfx) ) != sizeof(wfx) )return false;
	if( wfx.wFormatTag != WAVE_FORMAT_PCM )return false;
	wfx.cbSize = 0;

	if( mmioAscend( hmmio, &subChunk, 0 ) )return false;

	subChunk.ckid = mmioFOURCC( 'd', 'a', 't', 'a' );
	if( mmioDescend( hmmio, &subChunk, &mainChunk, MMIO_FINDCHUNK ) )return false;

	wavedata = (char*)malloc( subChunk.cksize );
	if( (DWORD)mmioRead( hmmio, (HPSTR)wavedata, subChunk.cksize ) == subChunk.cksize ){
		DSBUFFERDESC dsbdesc;
		ZeroMemory( &dsbdesc, sizeof(DSBUFFERDESC) );
		dsbdesc.dwSize = sizeof(DSBUFFERDESC);
		dsbdesc.dwFlags =	DSBCAPS_GETCURRENTPOSITION2 |
							DSBCAPS_GLOBALFOCUS |
							DSBCAPS_LOCDEFER |
							DSBCAPS_CTRLPAN |
							DSBCAPS_CTRLFREQUENCY |
							DSBCAPS_CTRLVOLUME;
		dsbdesc.dwBufferBytes = subChunk.cksize;
		dsbdesc.lpwfxFormat = &wfx;

		hr = lpDS->CreateSoundBuffer( &dsbdesc, &lpDSSecond[no], NULL );
		if( hr == DS_OK ){
			LPVOID pMem1,pMem2;
			DWORD dwSize1,dwSize2;
			lpDSSecond[no]->Lock( 0, dsbdesc.dwBufferBytes, &pMem1, &dwSize1, &pMem2, &dwSize2, 0 );

			memcpy( pMem1, wavedata, dwSize1 );
			if( dwSize2 )memcpy( pMem2, wavedata + dwSize1, dwSize2 );

			lpDSSecond[no]->Unlock( pMem1, dwSize1, pMem2, dwSize2 );
			free( wavedata );
			return true;
		}
	}
	free( wavedata );
	return false;
}

//WaveDataの読み込み
void LoadWave( int no, char *filename )
{
	HMMIO hmmio;
	
	hmmio = mmioOpen( filename, NULL, MMIO_ALLOCBUF | MMIO_READ );
	if( !hmmio )return;

	SetWave( hmmio, no );

	mmioClose( hmmio, 0 );
}

//Waveの再生
void PlayWave( int no, BOOL roop )
{
	if( lpDS == NULL )return;
	if( lpDSSecond[no] == NULL )return;

	lpDSSecond[no]->Stop();
	lpDSSecond[no]->SetCurrentPosition( 0 );
	if( roop == TRUE )lpDSSecond[no]->Play( 0, 0, DSBPLAY_LOOPING );
	else lpDSSecond[no]->Play( 0, 0, 0 );
}

//Waveの停止
void StopWave( int no )
{
	if( lpDS == NULL )return;
	if( lpDSSecond[no] == NULL )return;

	lpDSSecond[no]->Stop();
}

//ボリューム設定
long GetVolume( int no )
{
	long Volume;
	lpDSSecond[no]->GetVolume( &Volume );
	return Volume;
}

void SetVolume( int no, long Volume)
{
	lpDSSecond[no]->SetVolume( Volume );
}

//パンの設定(音の位置を変えられる)
long GetPan( int no )
{
	long pan;
	lpDSSecond[no]->GetPan( &pan );
	return pan;
}

void SetPan( int no, long  pan )
{
	lpDSSecond[no]->SetPan( pan );
}


//	MIDI
extern HWND hWnd;

//Midiファイルロード
void LoadMidiFile( LPSTR filename )
{
	TCHAR buf[260];
	StopMidi();
	CloseMidi();

	//コマンド文字列にファイル名を挿入
	wsprintf( buf, "open %s type sequencer alias BGM", filename );

	//ファイルオープンの文字列を送信
	mciSendString( buf, NULL, 0, NULL );
}

//Midi再生
void PlayMidi( bool loop )
{
	//再生場所を先頭に移動させる
	mciSendString( "seek BGM to start", NULL, 0, NULL );

	//ループが設定されているかどうか
	if( loop ){
		mciSendString( "play BGM notify", NULL, 0, hWnd );
	}else{
		mciSendString( "play BGM", NULL, 0, NULL );
	}
}

//再生の停止
void StopMidi()
{
	mciSendString( "stop BGM", NULL, 0, NULL );
}

//ファイルを閉じる
void CloseMidi()
{
	mciSendString( "close BGM", NULL, 0, NULL );
}