//lib
#pragma comment( lib, "Strmiids.lib" )

//include
#include "KX.h"
#include "system.h"
#include "dshow.h"

//GraphBuilderオブジェクトを作る
IGraphBuilder *pBuilder = NULL;
IMediaControl *pMediaControl = NULL;
IMediaEvent *pMediaEvent = NULL;
IBasicAudio *pAudio = NULL;
IVideoWindow *pVideoWindow = NULL;

extern HWND hWnd;

BOOL InitShow( HWND hWnd )
{
	//COMライブラリの初期化
	CoInitialize( NULL );

	return TRUE;
}

void ReleaseShow()
{
	if( pVideoWindow ){
		//非表示
		pVideoWindow->put_Visible( OAFALSE );
		//親ウインドウを切り離す
		pVideoWindow->put_Owner( NULL );
	}
	if( pBuilder )RELEASE( pBuilder );
	if( pMediaControl )RELEASE( pMediaControl );
	if( pMediaEvent )RELEASE( pMediaEvent );
	if( pAudio )RELEASE( pAudio );
	CoUninitialize();
}

void LoadMedia( LPSTR filename )
{
	StopMedia();

	CoCreateInstance( CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER,
							IID_IGraphBuilder, (void**)&pBuilder );
	WCHAR name[MAX_PATH];
	MultiByteToWideChar(CP_ACP, 0, filename, -1, name, MAX_PATH );

	pBuilder->RenderFile( name, NULL );
	pBuilder->QueryInterface( IID_IMediaControl, (void**)&pMediaControl );
	//描画ウインドウを操作する
	pBuilder->QueryInterface( IID_IVideoWindow, (void**)&pVideoWindow );
	//ウインドウモード・スクリーンモードの変更
	pVideoWindow->put_FullScreenMode( OAFALSE );
	//hWndは自分のウインドウ・ハンドルであると仮定する
	pVideoWindow->put_Owner( (OAHWND)hWnd );
	//ウインドウ・スタイルを変更し、子ウインドウとする
	pVideoWindow->put_WindowStyle( WS_CHILD | WS_CLIPSIBLINGS );

	SizeMedia( hWnd );
}

void SizeMedia( HWND hWnd )
{
	//自分のウインドウ・サイズを得る
	RECT rect;
	long destWidth, destHeight;
	GetClientRect( hWnd, &rect );
	destWidth = rect.right;
	destHeight = rect.bottom;
	//ビデオ画面のウインドウサイズを変更する
	pVideoWindow->SetWindowPosition( 0, 0, destWidth, destHeight );
}

int CheckState()
{
	if( pMediaControl!=NULL ){
		HRESULT hr;
		OAFilterState fs;
		hr = pMediaControl->GetState( 0, &fs );
		if( FAILED( hr ) )return FALSE;

		if( fs==State_Stopped )return 1;
		if( fs==State_Paused ) return 2;
		if( fs==State_Running )return 3;
	}
	return FALSE;
}

int WaitMedia()
{
	if( pBuilder!=NULL ){
		pBuilder->QueryInterface( IID_IMediaEvent, (void**)&pMediaEvent );

		long eventcode;
		pMediaEvent->WaitForCompletion( INFINITE, &eventcode );

		if( eventcode==EC_ERRORABORT )return 0;
		if( eventcode==EC_COMPLETE )return 1;
		if( eventcode==EC_USERABORT )return 2;
	}
	return 0;
}

void PlayMedia()
{
	if( pMediaControl!=NULL )pMediaControl->Run();
}

void StopMedia()
{
	if( pMediaControl!=NULL )pMediaControl->Stop();
}

void PauseMedia()
{
	if( pMediaControl!=NULL )pMediaControl->Pause();
}

void Volume( int vol )
{
	if( pBuilder!=NULL ){
		pBuilder->QueryInterface( IID_IBasicAudio, (void**)&pAudio );

		vol	-= 10000;
		pAudio->put_Volume( vol );
	}
}