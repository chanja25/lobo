#include "KX.h"
#include "system.h"

LPKX2DOBJ lpEnd = NULL;
LPKX2DOBJ lpEMoji = NULL;
int ecnt;

//初期化
void END_Initialize()
{
	lpEnd = KX_Load2DObject( "DATA\\END.png" );
	lpEMoji = KX_Load2DObject( "DATA\\ENTER.png" );

	ecnt = 0;

	dwMainMode = END_MODE;
}

void END_Release()
{
	KX_Release2DObject( lpEnd );
	KX_Release2DObject( lpEMoji );

	TITLE_Initialize();
}

//処理
void END_Proc()
{
	ecnt++;
	if( KEY(KEY_ENTER)==3 ){
		END_Release();
	}
}

//描画
void END_Draw()
{
	KX_Render2DObject( 0, 0, 640, 480, lpEnd, 0, 0, 640, 480 );
	if( ecnt%30<15 )KX_Render2DObject( 200, 320, 240, 64, lpEMoji, 0, 0, 240, 64 );
}