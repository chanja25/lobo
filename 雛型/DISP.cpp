#include "KX.h"
#include "system.h"

LPKX2DOBJ lpStatus = NULL;
LPKX2DOBJ lpStatus2 = NULL;
extern int hp;
extern int en;

//初期化
void DISP_Initialize()
{
	lpStatus = KX_Load2DObject( "DATA\\status.png", COLORKEY_LEFTTOP );
	lpStatus2 = KX_Load2DObject( "DATA\\status.png" );
}

void DISP_Release()
{
	KX_Release2DObject( lpStatus );
	KX_Release2DObject( lpStatus2 );
}

//処理
void DISP_Proc()
{
}

//描画
void DISP_Draw()
{
	KX_Render2DObject( 0,  0, 240, 50, lpStatus, 0, 30, 240, 50 );

	float w;
	w = (float)hp/HP_MAX;
	KX_Render2DObject( 50, 18, (int)(134*w), 10, lpStatus2, 0, 0, 1, 1, 0xff8888ff );
	w = (float)en/ENERGY_MAX;
	KX_Render2DObject( 50, 31, (int)(134*w), 10, lpStatus2, 0, 0, 1, 1, 0xffff8888 );
}