//include
#include "KX.h"
#include "system.h"

LRESULT CALLBACK WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );

HWND hWnd;

static int ifg;

int APIENTRY WinMain( HINSTANCE hInst, HINSTANCE hPrev, LPSTR lpCmd, int nCmd )
{
	WNDCLASSEX wcex;
	MSG msg;

	wcex.cbSize			= sizeof( WNDCLASSEX );
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= ( WNDPROC )WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInst;
	wcex.hIcon			= LoadIcon( NULL, IDI_APPLICATION );
	wcex.hCursor		= LoadCursor( NULL,IDC_ARROW );
	wcex.hbrBackground	= ( HBRUSH )GetStockObject( WHITE_BRUSH );
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "���^";
	wcex.hIconSm		= LoadIcon( NULL,IDI_APPLICATION );
	
	RegisterClassEx( &wcex );

	hWnd = CreateWindow(
		wcex.lpszClassName,
		"���^",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		640,
		480,
		NULL,
		NULL,
		hInst,
		NULL
	);

	ShowWindow( hWnd, nCmd );
	UpdateWindow( hWnd );

	KX_Init( hWnd );
	ifg = 1;

	SYS_Initialize();

	while( 1 ){
		if( PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) ){
			if(  !GetMessage( &msg, NULL, 0, 0 ) )break;
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}else{
			if(SYS_Proc())SYS_Draw();
		}
	}

	KX_Release();

	return 0;
}

LRESULT CALLBACK WndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch( msg ){
		case WM_CREATE:
			ifg = 0;
			return 0;
		case WM_KEYDOWN:
			switch( wParam ){
				case VK_ESCAPE:
					PostMessage( hWnd, WM_CLOSE, 0, 0);
					return 0;
			}
			break;
		case WM_SIZE:
			//if( ifg==1 )SizeMedia( hWnd );
			return 0;
		case WM_DESTROY:
			PostQuitMessage( 0 );
			return 0;
	}
	return DefWindowProc( hWnd, msg, wParam, lParam );
}