//include
#include "KX.h"
#include "system.h"


void MAIN_Initialize()
{
	ENEMY_Initialize();
	STAGE_Initialize();
	PLAYER_Initialize();
	SHOT_Initialize();
	DISP_Initialize();

	dwMainMode = MAIN_MODE;
}

void MAIN_Release()
{
	STAGE_Release();
	PLAYER_Release();
	SHOT_Release();
	DISP_Release();
	ENEMY_Release();
}

void MAIN_Proc()
{
	STAGE_Proc();
	PLAYER_Proc();
	SHOT_Proc();
	DISP_Proc();
	ENEMY_Proc();
}

void MAIN_Draw()
{
	STAGE_Draw();
	PLAYER_Draw();
	SHOT_Draw();
	DISP_Draw();
	ENEMY_Draw();
}