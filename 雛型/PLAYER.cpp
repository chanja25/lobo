//include
#include "KX.h"
#include "system.h"

LPKX2DOBJ lpPlayer = NULL;
int hp,en;
int x,y;
int MoveX,MoveY;
int px,py;
int motion, angle;
int cnt;

extern int MapX, MapY;
extern int ex[ENEMY_MAX],ey[ENEMY_MAX];
extern int emy[ENEMY_MAX];
extern int eangle[ENEMY_MAX];
extern int estate[ENEMY_MAX];

void PLAYER_Initialize()
{
	hp = HP_MAX;
	en = ENERGY_MAX;
	x = 128;
	y = 416;
	MoveX = MoveY = 0;
	motion = 0;
	angle = 1;
	cnt = 0;
	lpPlayer = KX_Load2DObject( "DATA\\Robo.png", COLORKEY_LEFTTOP );
}

void PLAYER_Release()
{
	KX_Release2DObject( lpPlayer );
}

void PLAYER_MOVE()
{
	if( motion>=4 && motion<= 6 ){
		if(cnt>=5){
			if(motion==6 && cnt>=8){
				motion = 0;
				cnt = 0;
			}else if(motion<6){
				motion++;
				cnt = 0;
			}
		}
		if( MoveX>0 )MoveX -= 3;
		if( MoveX<0 )MoveX += 3;
		if( abs(MoveX)<1 )MoveX = 0;
		cnt++;
	}else if( KEY(KEY_C) ){
		motion = 7;
		if( MoveX>0 )MoveX -= 4;
		if( MoveX<0 )MoveX += 4;
		if( abs(MoveX)<1 )MoveX = 0;
	}else{
		if( KEY(KEY_LEFT) ){
			MoveX -= 4;
			motion = 1;
			angle = 0;
		}else if( KEY(KEY_RIGHT) ){
			MoveX += 4;
			motion = 1;
			angle = 1;
		}else{
			if( MoveX>0 )MoveX -= 4;
			if( MoveX<0 )MoveX += 4;
			if( abs(MoveX)<1 )MoveX = 0;
			motion = 0;
		}

		if( KEY(KEY_Z) ){
			motion = 2;
		}
		if( KEY(KEY_X)==3 ){
			motion = 4;
		}
	}

	if( motion==2 ){
			if( GameCnt%5==0 )SHOT_Create( x, y, angle, 1, 0 );
	}
	if( motion==7 ){
			if( GameCnt%30==0 )SHOT_Create( x, y, angle, 2, 0 );
	}

	if( KEY(KEY_LSHIFT) ){
		if( MoveX>64 )MoveX = 64;
		if( MoveX<-64 )MoveX = -64;
	}else{
		if( MoveX>40 )MoveX = 40;
		if( MoveX<-40 )MoveX = -40;
	}
	if( x>=350 && MoveX>0 && MapX>-2560 ){
		MapX -= MoveX/8;
		for( int i=0;i<ENEMY_MAX;i++ ){
			if( estate[i] )ex[i] -= MoveX/8;
		}
	}else if( x<=150 && MoveX<0 && MapX<0 ){
		MapX -= MoveX/8;
		for( int i=0;i<ENEMY_MAX;i++ ){
			if( estate[i] )ex[i] -= MoveX/8;
		}
	}else x += MoveX/8;

	if( x<32 && MoveX<0 ){
		x = 32;
		MoveX = 0;
	}

	if( MapX>0 )MapX = 0;
	if( MapX<-2560 )MapX = -2560;

	if( KEY(KEY_SPACE)==3 ){
		if( MoveY==0 )MoveY = -10;
	}else if( KEY(KEY_SPACE) ){
		if( en>0 ){
			if( MoveY>-5 )MoveY -= 2;
			en -= 2;
		}
	}else{
		en += 1;
		if( MoveX==0 )en += 1;

		if(en>ENERGY_MAX)en = ENERGY_MAX;
	}

	if( MoveY<-5 )MoveY++;

	y += MoveY;
	MoveY++;
	if( MoveY>15 )MoveY = 15;

	if( MoveY>0 )CheckWallDown( &x, &y, &MoveY );
	if( MoveY<0 )CheckWallUp( &x, &y, &MoveY );
	if( MoveX<0 )CheckWallLeft( &x, &y, &MoveX );
	if( MoveX>0 )CheckWallRight( &x, &y, &MoveX );
	
	if( x>700 && MoveX>0 ){
		x = 700;
		MAIN_Release();
		CLEAR_Initialize();
	}
	if( hp<=0 ){
		MAIN_Release();
		END_Initialize();
	}
	if( y>540 ){
		MAIN_Release();
		END_Initialize();
	}
	
	for( int i=0;i<ENEMY_MAX;i++ ){
		if( estate[i]==0 )continue;
		if( ((x+20>=ex[i]-20 && x-20<=ex[i]+20) || (x-20<=ex[i]+20 && x+20>=ex[i]-20)) && ( y>=ey[i]-60 && y<=ey[i]-50 ) ){
			MoveY = 0;
		}else{
			if( x+20>ex[i]-20 && x-20<ex[i]+20 && angle==0 ){
				if( y>ey[i]-55 && y-55<ey[i] )x = ex[i]+40;
			}
			if( x-20<ex[i]+20 && x+20>ex[i]-20 && angle==1 ){
				if( y>ey[i]-55 && y-55<ey[i] )x = ex[i]-40;
			}
		}
		for( int j=0;j<ENEMY_MAX;j++ ){
			if( estate[j]==0 )continue;
			if( i==j )continue;
			if( ((ex[j]+20>=ex[i]-20 && ex[j]-20<=ex[i]+20) || (ex[j]-20<=ex[i]+20 && ex[j]+20>=ex[i]-20)) && ( ey[j]>=ey[i]-60 && ey[j]<=ey[i]-50 ) ){
				emy[j] = 0;
			}else{
				if( ex[j]+20>ex[i]-20 && ex[j]-20<ex[i]+20 && eangle[j]==0 ){
					if( ey[j]>ey[i]-55 && ey[j]-55<ey[i] )ex[j] = ex[i]+40;
				}
				if( ex[j]-20<ex[i]+20 && ex[j]+20>ex[i]-20 && eangle[j]==1 ){
					if( ey[j]>ey[i]-55 && ey[j]-55<ey[i] )ex[j] = ex[i]-40;
				}
			}
		}
	}
}

void PLAYER_Proc()
{
	PLAYER_MOVE();
}

void PLAYER_Draw()
{
	KX_Render2DObject( x-32, y-64, 64, 64, lpPlayer, motion*64, angle*64, 64, 64 ); 
}