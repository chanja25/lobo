#include "KX.h"
#include "system.h"

LPKX2DOBJ lpClear = NULL;
LPKX2DOBJ lpCMoji = NULL;
int ccnt;

//初期化
void CLEAR_Initialize()
{
	lpClear = KX_Load2DObject( "DATA\\CLEAR.png" );
	lpCMoji = KX_Load2DObject( "DATA\\ENTER.png" );

	ccnt = 0;

	dwMainMode = CLEAR_MODE;
}

void CLEAR_Release()
{
	KX_Release2DObject( lpClear );
	KX_Release2DObject( lpCMoji );

	TITLE_Initialize();
}

//処理
void CLEAR_Proc()
{
	ccnt++;
	if( KEY(KEY_ENTER)==3 ){
		CLEAR_Release();
	}
}

//描画
void CLEAR_Draw()
{
	KX_Render2DObject( 0, 0, 640, 480, lpClear, 0, 0, 640, 480 );
	if( ccnt%30<15 )KX_Render2DObject( 200, 320, 240, 64, lpCMoji, 0, 0, 240, 64 );
}