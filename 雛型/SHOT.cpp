#include "KX.h"
#include "system.h"

#define SHOT_MAX 32

LPKX2DOBJ lpShot[2];
int sx[SHOT_MAX], sy[SHOT_MAX];
int angle[SHOT_MAX];
int state[SHOT_MAX];
int damage[SHOT_MAX];
int Type[SHOT_MAX];

extern int hp;
extern int x,y;
extern int ehp[ENEMY_MAX];
extern int ex[ENEMY_MAX],ey[ENEMY_MAX];
extern int estate[ENEMY_MAX];

//初期化
void SHOT_Initialize()
{
	lpShot[0] = KX_Load2DObject( "DATA\\tama.png", COLORKEY_LEFTTOP );
	lpShot[1] = KX_Load2DObject( "DATA\\misail.png", COLORKEY_LEFTTOP );
	for( int i=0;i<SHOT_MAX;i++ ){
		sx[i] = sy[i] = 0;
		angle[i] = 0;
		state[i] = 0;
		Type[i] = 0;
	}
}

void SHOT_Release()
{
	for( int i=0;i<2;i++ ){
		KX_Release2DObject( lpShot[i] );
	}
}

void SHOT_Hit( int n )
{
	if( Type[n]==1 ){
		if( x-20<sx[n]-5 && y-60<sy[n]-5 ){
			if( x+20>sx[n]+5 && y>sy[n]+5 ){
				hp -= damage[n]/2;
				state[n] = 0;
			}
		}
	}else{
		for( int i=0;i<ENEMY_MAX;i++ ){
			if( estate[i]==0 )continue;
			if( ex[i]-20<sx[n]-5 && ey[i]-60<sy[n]-5 ){
				if( ex[i]+20>sx[n] && ey[i]>sy[n]+5 ){
					ehp[i] -= damage[n];
					state[n] = 0;
				}
			}
		}
	}
}

void SHOT_TAMA( int n )
{
	if( angle[n]==0 )sx[n] -= 10;
	else sx[n] += 10;
	if( sx[n]>640 )state[n] = 0;
	if( sx[n]<-32 )state[n] = 0;
}

void SHOT_MISSILE( int n )
{
	if( angle[n]==0 )sx[n] -= 8;
	else sx[n] += 8;
	if( sx[n]>640 )state[n] = 0;
	if( sx[n]<-32 )state[n] = 0;
}

void SHOT_Create( int x, int y, int Angle, int State, int type )
{
	for( int i=0;i<SHOT_MAX;i++ ){
		if( state[i]==0 ){
			if( State==1 ){
				sx[i] = x;
				if( Angle==0 )sx[i] -= 30;
				else sx[i] += 25;
				sy[i] = y-35;
				angle[i] = Angle;
				state[i] = State;
				Type[i] = type;
				damage[i] = 5;
				for( int j=0;j<SHOT_MAX;j++ ){
					if( state[j]==0 ){
						sx[j] = x;
						if( Angle==0 )sx[j] -= 30;
						else sx[j] += 25;
						sy[j] = y-45;
						angle[j] = Angle;
						state[j] = State;
						damage[j] = 5;
						Type[j] = type;
						break;
					}
				}
				break;
			}else{
				sx[i] = x;
				if( Angle==0 )sx[i] -= 20;
				else sx[i] += 15;
				sy[i] = y-50;
				angle[i] = Angle;
				state[i] = State;
				damage[i] = 50;
				Type[i] = type;
				break;
			}
		}
	}
}

//処理
void SHOT_Proc()
{
	for( int i=0;i<SHOT_MAX;i++ ){
		if( state[i]==1 )SHOT_TAMA(i);
		if( state[i]==2 )SHOT_MISSILE(i);
		if( state[i] )SHOT_Hit(i);
	}
}

//描画
void SHOT_Draw()
{
	for( int i=0;i<SHOT_MAX;i++ ){
		if( state[i]==1 )KX_Render2DObject( sx[i]-5, sy[i]-5, 10, 10, lpShot[0], 0, 0, 10, 10 );
		if( state[i]==2 )KX_Render2DObject( sx[i]-32, sy[i]-32, 64, 64, lpShot[1], 0, angle[i]*64, 64, 64 );
	}
}